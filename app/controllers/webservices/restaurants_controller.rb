class Webservices::RestaurantsController <  WebservicesController 

#   api :GET, '/places/getConcepts'
#   formats ['json']
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

#   def getConcepts
#     render :json => Concept.mapConcepts(Concept.where(:enabled => true))
#   end
  
#   api :POST, '/places/createFavorite'
#   formats ['json']
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "id ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def createFavorite
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       current_user.favorites.where(:recipe_id => params[:recipe_id].to_s).destroy_all
#       f = Favorite.new
#       f.user = current_user
#       f.recipe = recipe
#       f.save
#       render :json => {}
#     end  
#   end

#   api :DELETE, '/places/deleteFavorite'
#   formats ['json']
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "id ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def deleteFavorite
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       current_user.favorites.where(:recipe_id => params[:recipe_id].to_s).destroy_all
#       render :json => {}
#     end  
#   end

#   api :GET, '/places/getFavorites'
#   formats ['json']
#   param :page, :number, :desc => "Página atual de lojas", :required => true, :missing_message => lambda { "page ausente" }
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

#   def getFavorites
#     recipes = []
#     current_user.favorites.skip(params[:page].to_i * 10).limit(10).each do |f|
#       recipes << f.recipe
#     end
#     render :json => Recipe.mapRecipes(recipes, current_user)
#   end

#   api :GET, '/places/getRecipesMap'
#   formats ['json']
#   param :swX, String, :desc => "south west X"
#   param :swY, String, :desc => "south west Y"
#   param :neX, String, :desc => "north east X"
#   param :neY, String, :desc => "north east Y"
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

 
#   def getRecipesMap
#     # render :json => Recipe.mapRecipesMap(Recipe.toPins(params[:swX],params[:swY],params[:neX],params[:neY]))
#     render :json => Recipe.mapRecipesMap(Recipe.all)
#   end


#   api :GET, '/places/getClosestRecipes'
#   formats ['json']
#   param :page, :number, :desc => "Página atual de lojas", :required => true, :missing_message => lambda { "page ausente" }
#   # param :latitude, :number, :desc => "Latitude", :required => true, :missing_message => lambda { "lat ausente" }
#   # param :longitude, :number, :desc => "Longitude", :required => true, :missing_message => lambda { "lon ausente" }
#   param :search, String, :desc => "Concept id", :required => false
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

#   def getClosestRecipes
#     source = params[:search].nil? ? Recipe.all : Recipe.where(:name => /.*#{params[:search]}.*/i)
#     render :json => Recipe.mapRecipes(source.skip(params[:page].to_i * 10).limit(10), current_user)
#   end


#   api :GET, '/places/getRecipesByConcept'
#   formats ['json']
#   param :page, :number, :desc => "Página atual de lojas", :required => true, :missing_message => lambda { "page ausente" }
#   param :concept_id, String, :desc => "Concept id", :required => true, :missing_message => lambda { "categoria ausente" }
#   param :search, String, :desc => "Concept id", :required => false
#   # param :latitude, :number, :desc => "Latitude", :required => true, :missing_message => lambda { "lat ausente" }
#   # param :longitude, :number, :desc => "Longitude", :required => true, :missing_message => lambda { "lon ausente" }

#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

#   def getRecipesByConcept
#     source = params[:search].nil? ? Recipe.all : Recipe.where(:name => /.*#{params[:search]}.*/i)
#     render :json => Recipe.mapRecipes(source.where(:concept_id => params[:concept_id]).skip(params[:page].to_i * 10).limit(10), current_user)
#   end



#   api :GET, '/places/findRecipe'
#   formats ['json']
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "id ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def findRecipe
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       render :json => Recipe.findRecipe([recipe], current_user).first
#     end  
#   end

#   api :GET, '/places/getIngredients'
#   formats ['json']
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "cep ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def getIngredients
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       render :json => Ingredient.mapIngredients(recipe.recipes)
#     end  
#   end

#   api :GET, '/places/getPayments'
#   formats ['json']
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "cep ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def getPayments
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       render :json => Payment.mapPayments(recipe.payments)
#     end  
#   end


#   api :GET, '/places/getRatings'
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "cep ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def getRatings
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       render :json => Rating.mapRatings(recipe.ratings)
#     end  
#   end

#   api :POST, '/places/createRating'
#   param :recipe_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "cep ausente" }
#   param :message, String, :desc => "Message", :required => true, :missing_message => lambda { "cep ausente" }
#   param :stars, :number, :desc => "Message", :required => true, :missing_message => lambda { "cep ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def createRating
#     recipe = Recipe.find(params[:recipe_id].to_s)
#     if recipe.nil?
#       render :nothing => true, status: 340
#     else
#       r =  Rating.new
#       if current_user.ratings.where(:recipe_id => params[:recipe_id].to_s).count > 0
#         r = current_user.ratings.where(:recipe_id => params[:recipe_id].to_s).first
#       end
#       r.recipe = recipe
#       r.message = params[:message]
#       r.stars = params[:stars]
#       r.user = current_user
#       r.save
#       render :json => Rating.mapRatings([r]).first
#     end  
#   end

#   api :DELETE, '/places/deleteRating'
#   formats ['json']
#   param :rating_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "id ausente" }
#   error 401, "Usuário não logado"
#   error 340, "rating não pertence ao usuário"
#   error 500, "Erro desconhecido"

#   def deleteRating
#     render :json => {}
#   end

#   api :PUT, '/places/updateRating'
#   param :rating_id, String, :desc => "Recipe id", :required => true, :missing_message => lambda { "id ausente" }
#   param :message, String, :desc => "Message", :required => true, :missing_message => lambda { "cep ausente" }
#   param :stars, :number, :desc => "Message", :required => true, :missing_message => lambda { "cep ausente" }
#   error 401, "Usuário não logado"
#   error 340, "nenhuma loja encontrada"
#   error 500, "Erro desconhecido"

#   def updateRating
#     render :json => {}
#   end

end