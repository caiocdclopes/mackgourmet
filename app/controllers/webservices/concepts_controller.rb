class Webservices::ConceptsController <  WebservicesController

  api :GET, '/concepts/getConcepts'
  description "Esse método retorna todos o conceitos no formato Json."
  formats ['json']
  example "{
  'id': string,
  'name': string,
  'description': string,
  'picture': string
}"
  error 401, "Usuário não logado"
  error 500, "Erro desconhecido"
  def getConcepts
    render :json => Concept.mapConcepts(Concept.all)
  end

#   api :GET, '/orders/getMackGourmets'
#   formats ['json']
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"
#   def getMackGourmets
#     render :json => {ekkos: 100.0}
#   end

#   api :GET, '/orders/getMackGourmetMonitor'
#   formats ['json']
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"
#   def getMackGourmetMonitor
#     render :json => {ekkos: 121.0}
#   end

#   api :GET, '/orders/getPendingMackGourmets'
#   formats ['json']
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"
#   def getPendingMackGourmets
#     render :json => {ekkos: 100.0}
#   end



#   api :GET, '/orders/getOrders'
#   formats ['json']
#   param :month_id, String, :desc => "Month ID", :required => true, :missing_message => lambda { "mes ausente" }
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"
#   def getOrders
#     render :json => Order.mapOrders(current_user.months.find(params[:month_id]).orders)
#   end

#   api :GET, '/orders/getChart'
#   formats ['json']
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

#   def getChart
#     groups = {}
#     Order.mapOrdersChart(current_user.orders.where(:created_at.gte => DateTime.now - 15.days ).asc(:date)).each do |order|
#       date = order[:date]
#       amount = order[:amount_ekko]
#       if !groups.keys.include?(date)
#         groups[date] = 0
#       end
#       groups[date] += amount
#     end
#     array = []
#       groups.keys.each do |key|
#         json = {}
#         json[:date] = key
#         json[:ekkos] = groups[key]
#         array << json
#       end
#     render :json => {:from => "18/01/1990", :to => "22/02/1990", :points => array, :orders => 10, :total => 200.0 } 
#   end

#   api :GET, '/orders/getChartConcepts'
#   formats ['json']
#   error 401, "Usuário não logado"
#   error 500, "Erro desconhecido"

#   def getChartConcepts
#     array = []
#     Concept.all.limit(3).each do |c|
#       json = {}
#       json[:concept] = Concept.mapConcept(c)
#       json[:amount] = 100.0
#       array << json
#     end
#     render :json => array

#   end

  

end
