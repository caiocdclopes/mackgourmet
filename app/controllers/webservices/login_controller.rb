class Webservices::LoginController <  WebservicesController 

  api :POST, '/login/forgotPass'
  formats ['json']
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  error 340, "Usuário não encontrado"
  error 500, "Erro desconhecido"

  def forgotPass
    if User.where(:email => params[:email]).count > 0
      render :json => {}
    else
      render :nothing => true, :status => 340
    end
  end

  api :POST, '/login/signinFacebook'
  param :name, String, :desc => "Name", :required => true, :missing_message => lambda { "nome ausente" }
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  param :facebook, String, :desc => "Facebook", :required => true, :missing_message => lambda { "facebook ausente" }
  error 401, "Usuário não encontrado"
  error 500, "Erro desconhecido"

  def signinFacebook
    u = User.where(:email => params[:email]).first
    if !u.nil?
      u.facebook = (params[:facebook].nil? || params[:facebook].to_s == "empty") ? u.facebook :  params[:facebook]
      sign_in u, :bypass => true
      u.save(validate: false)
      render :json =>  {:phone => u.phone, :error_code => "0", :status => u.id,  :picture => (u.facebook.nil? || u.changedPhoto) ?  u.picture.url(:original) : "http://graph.facebook.com/#{u.facebook}/picture?type=large", :name => u.name}.to_json
    elsif u.nil? && !params[:facebook].nil? 
      user = User.new
      user.password = params[:facebook]
      user.password_confirmation = params[:facebook]
      user.facebook = params[:facebook]
      user.name = params[:name]
      user.email = params[:email]
      user.save(validate: false)
      sign_in user, :bypass => true
      u = user
      render :json => User.mapUser(u)
    else
      render :nothing => true, :status => 401
    end
  end

  api :POST, '/login/signin'
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  param :password, String, :desc => "Password", :required => true, :missing_message => lambda { "senha ausente" }
  error 401, "Usuário não encontrado"
  error 500, "Erro desconhecido"

  def signin
    u = User.where(:email => params[:email]).first
    if !u.nil?
      if u.valid_password?(params[:password]) 
        sign_in u, :bypass => true
        u.save(validate: false)
        render :json => User.mapUser(u)
      else
        render :nothing => true, :status => 401
      end
    else
      render :nothing => true, :status => 401
    end
  end

  api :POST, '/login/signup'
  formats ['json']
  example "{
  id : string,
  name : string,
  email : string,
  picture : string,
  type : string,
  created_at : string
}"
  param :email, String, :desc => "Email", :required => true, :missing_message => lambda { "email ausente" }
  param :password, String, :desc => "Password", :required => true, :missing_message => lambda { "senha ausente" }
  param :name, String, :desc => "Name", :required => true, :missing_message => lambda { "nome ausente" }
  param :profile_picture, String, :desc => "Usar multipart/form-data para subir imagem", :required => false, :allow_nil => true
  param :photo_64, String, :desc => "Base64 string, only for Android", :required => false, :allow_nil => true

  error 403, "Usuário já cadastrado"
  error 409, "Parametros invalidos"
  error 500, "Erro desconhecido"

  def signup

    u = User.where(:email => params[:email].downcase).first
    if !u.nil?
        render :json =>  {:message => "usuário já cadastrado"}, :status => 403
    else
      user = User.new
      user.name = params[:name]
      user.email = params[:email].downcase
      user.password = params[:password]
      user.password_confirmation = params[:password]
      user.picture = params[:profile_picture]

      if !params[:photo_64].nil?
        tempfile = Tempfile.new("fileupload")
        tempfile.binmode
        tempfile.write(Base64.decode64(params[:photo_64]))
        uploaded_file = ActionDispatch::Http::UploadedFile.new(:tempfile => tempfile, :filename => "temp", :original_filename => "temp2") 
        user.picture =  uploaded_file
      end

      if !user.valid?
        render :json => {:message => "Parametros inválidos", "errors" => user.errors.messages}, :status => 409
      else
        user.save
        sign_in user, :bypass => true
        render :json =>  User.mapUser(user)
      end
    end
  end


end
