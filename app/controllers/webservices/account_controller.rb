class Webservices::AccountController <  WebservicesController

  api :POST, '/account/updatePassword'
  formats ['json']
  param :current_password, String, :desc => "Current Password", :required => true, :missing_message => lambda { "senha atual ausente" }
  param :password, String, :desc => "New password", :required => true, :missing_message => lambda { "nova senha ausente" }
  param :password_confirmation, String, :desc => "New password confirmation", :required => true, :missing_message => lambda { "confirmação de senha ausente" }
  error 403, "parâmetros inválidos"
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"

  def updatePassword
    u = current_user
    user_id = current_user.id
    if params[:password].length < 8
      render :json => {message: "A senha deve ter 8 caracteres"}, :status => 403
    elsif params[:password] != params[:password_confirmation]
      render :json => {message: "Senha atual e confirmação diferentes"}, :status => 403
    elsif ! u.valid_password?(params[:current_password])
      render :json => {message: "Senha atual inválida"}, :status => 403
    elsif u.update_with_password(params)
      sign_in current_user, :bypass => true
      render :json =>  {:error_code => "0"}.to_json
    else
      render :nothing => true, :status => 401
    end
  end

  api :POST, '/account/updatePhoto'
  param :photo_64, String, :desc => "Base64 string, only for Android", :required => false, :allow_nil => true
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"

  def updatePhoto
    u = current_user
    u.picture = params[:photo]
    if !params[:photo_64].nil?
     tempfile = Tempfile.new("fileupload")
     tempfile.binmode
     tempfile.write(Base64.decode64(params[:photo_64]))
     uploaded_file = ActionDispatch::Http::UploadedFile.new(:tempfile => tempfile, :filename => "temp", :original_filename => "temp2") 
     u.picture =  uploaded_file
    end
    u.changedPhoto = true
    u.save(validate: false)
    render :json =>  User.mapUser(u).to_json
  end

  api :PUT, '/account/updateAbout'
  param :gender, String, :desc => "gender", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :phone, String, :desc => "User Telephone", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :name, String, :desc => "User Name", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :email, String, :desc => "email", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :cpf, String, :desc => "cpf Name", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :birth_date, String, :desc => "birth_date", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :zip, String, :desc => "zip", :required => false, :missing_message => lambda { "telefone ausente" } 
  param :ekko_pass, String, :desc => "ekko_pass", :required => false, :missing_message => lambda { "telefone ausente" } 
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"

  def updateAbout
    u = current_user
    u.phone = params[:gender].nil? ? u.gender : params[:gender]
    u.phone = params[:email].nil? ? u.email : params[:email]
    u.phone = params[:phone].nil? ? u.phone : params[:phone]
    u.name = params[:name].nil? ? u.name : params[:name]
    u.cpf = params[:cpf].nil? ? u.cpf.gsub(".","").gsub("-","") : params[:cpf].gsub(".","").gsub("-","")
    u.zip = params[:zip].nil? ? u.zip : params[:zip]
    u.ekko_pass = params[:ekko_pass].nil? ? u.ekko_pass : params[:ekko_pass]
    u.birth_date = params[:birth_date].nil? ? u.birth_date : params[:birth_date]
    u.save(validate: false)
    render :json =>  User.mapUser(u).to_json
  end

  api :GET, '/account/getAbout'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"
  def getAbout
    a = User.mapUser(current_user)
    render :json => a.to_json
  end

  api :GET, '/account/getNotifications'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"

  def getNotifications
    render :json => current_user.notifications
  end
  
  api :DELETE, '/account/deleteNotification'
  formats ['json']
  error 401, "Usuário não autenticado"
  error 500, "Erro desconhecido"

  param :notification, String

  def deleteNotification
    Notification.find(params[:notification]).destroy
    render :json => {}
  end

end
