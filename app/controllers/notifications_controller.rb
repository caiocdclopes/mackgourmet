class NotificationsController < ApplicationController
  # GET /companies
  # GET /companies.json
  def index
    @notifications = Notification.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @companies }
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @notification = Notification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /companies/new
  # GET /companies/new.json
  def new
    @notification = Notification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @notification }
    end
  end



  # POST /companies
  # POST /companies.json
  def create
    notification = Notification.new(params[:notification])
    @users = []
    if notification.service == "Todos"
      @users = User.all
    elsif notification.service == "Lojas"
      @users = Recipe.all
    else
      @users = User.where(:_type.ne => "Recipe")
    end

    @users.each do |user|
      data = { :alert => notification.message, sound:  "default"}
      pushQ = Parse::Push.new(data)
      query = Parse::Query.new(Parse::Protocol::CLASS_INSTALLATION).eq(user._type == "Recipe" ?  'recipe_id' : "customer_id", user.id)
      pushQ.where = query.where
      pushQ.save
    end

    notification.save




    redirect_success("Notificação criada com sucesso!",:notifications, :index)
  end

 # GET /companies/1/edit
  def edit
    @notification = Notification.find(params[:id])
  end



  # PUT /companies/1
  # PUT /companies/1.json
  def update
    @company = Notification.find(params[:id])
    respond_to do |format|
      if @company.update_attributes(params[:notification])
        format.html { redirect_success("Notificação alterada com sucesso!",:notifications, :index)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company = Notification.find(params[:id])
    @company.destroy

    respond_to do |format|
        format.html { redirect_success("Notificação removida com sucesso!",:notifications, :index)}
      format.json { head :no_content }
    end
  end
end
