class RecipeMethodsController < ApplicationController
  # GET /methods
  # GET /methods.json
  def index
    @recipeMethods = []
    if params[:recipe]
      @recipe = Recipe.find(params[:recipe])
      @recipeMethods = @recipe.recipeMethods 
    else
      @recipeMethods = RecipeMethod.all
    end
  end

  # GET /methods/1
  # GET /methods/1.json
  def show
    @recipeMethod = RecipeMethod.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recipeMethod }
    end
  end

  # GET /methods/new
  # GET /methods/new.json
  def new
    @recipeMethod = RecipeMethod.new
    if params[:recipe]
      @recipe = Recipe.find(params[:recipe])
    else
      render_404
    end
  end

  # GET /methods/1/edit
  def edit
    @recipeMethod = RecipeMethod.find(params[:id])
    @recipe = @recipeMethod.recipe
  end

  # POST /methods
  # POST /methods.json
  def create
    @recipeMethod = RecipeMethod.new(params[:recipe_method])
    respond_to do |format|
      if @recipeMethod.save
        format.html { redirect_success_index_show("RecipeMethod criada com sucesso!",:recipe_methods, @recipeMethod.recipe.id)}
        format.json { render json: @recipeMethod, status: :created, location: @recipeMethod }
      else
        format.html { render action: "new" }
        format.json { render json: @recipeMethod.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /methods/1
  # PUT /methods/1.json
  def update
    @recipeMethod = RecipeMethod.find(params[:id])

    respond_to do |format|
      if @recipeMethod.update_attributes(params[:recipe_method])
        format.html { redirect_success_index_show("RecipeMethod alterado com sucesso!",:recipe_methods, @recipeMethod.recipe.id)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recipeMethod.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /methods/1
  # DELETE /methods/1.json
  def destroy
    @recipeMethod = RecipeMethod.find(params[:id])
    @recipeMethod.destroy

    respond_to do |format|
      format.html { redirect_success_index_show("RecipeMethod removido com sucesso!",:recipe_methods, @recipeMethod.recipe.id)}
      format.json { head :no_content }
    end
  end
end
