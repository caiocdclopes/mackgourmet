class HomeController < ApplicationController

	def index

 	end

  def download
	@data  = Date.today
	@dataFim = Date.today

	if params[:data_inicio]
		@data = params[:data_inicio].to_date
	end

	if @data.nil?
		@data = Date.today 
	end

	if params[:data_fim]
		@dataFim = params[:data_fim].to_date 
	end

	if @dataFim.nil?
		@dataFim = Date.today
	end


	if @data == @dataFim
		@dataFim = @dataFim + 1.days
	end
	@data = @data + 3.hours

	@user = nil
	@recipe = nil
	tryMateria = false
	tryProfessor = false

	if !params[:recipe].nil? && params[:recipe].length > 1
		tryProfessor = true
		   	 @recipe = Recipe.find(params[:recipe])
	end

	@orders = []
	if !@recipe.nil?
		@orders = @recipe.orders.where( :paidToRecipe => false)
	elsif tryProfessor && @recipe.nil?
		@orders = Order.where(:created_at => nil, :user_id.ne => nil)
	elsif tryMateria
		@orders = Order.where(:created_at.gte => @data, :created_at.lte => @dataFim, :user => @user, :user_id.ne => nil)
	else
		@orders = Order.where(:created_at.gte => @data, :created_at.lte => @dataFim, :user_id.ne => nil)
	end

	if @recipe.nil?
		@recipe = ""
	elsif !params[:recipe].nil? 
		@recipe = params[:recipe]
	else
		@recipe = @recipe.name
	end

	if @user.nil?
		@user = ""
	end
    alunos = "ID,Data,Status,Prestador,Loja,Cliente,Serviço,Valor,Valor MackGourmet\n"
    @orders.each do |aluno|
      alunos +=  aluno.id.to_s +  "," 
      alunos += aluno.date.strftime("%d/%m/%Y %H:%M") +  "," 
      alunos += aluno.status +  "," 
      alunos += aluno.recipe.provider.name + "," 
      alunos += aluno.recipe.name + "," 
      alunos += aluno.user.name + "," 
      alunos += aluno.service.name + "," 
      alunos += (aluno.amount/100).to_s + "," 
      alunos += ((aluno.service.transfer_percent/100) *  aluno.amount/100).to_s + "," 
      alunos += "\n"
    end

    send_data alunos, :filename => 'pedidos.csv' 

  end

  def gerar_boleto
  	o = Order.find(params[:order])
	transaction = PagarMe::Transaction.new(
	amount:         o.amount,    # in cents
    payment_method: 'boleto'
	  )
  	transaction.charge
  	redirect_to transaction.boleto_url     # => boleto's URL
  end


end