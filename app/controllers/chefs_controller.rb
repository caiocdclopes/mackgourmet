class ChefsController < ApplicationController
  # GET /concepts
  # GET /concepts.json
  def index
    @chefs = []
    if current_user.isStudent?
      @chefs = Chef.where(:concept => current_user.concept.id)
    else
      @chefs = Chef.all
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @chefs }
    end
  end

  # GET /concepts/1
  # GET /concepts/1.json
  def show
    @chef = Chef.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: { profissionais: @chef } }
    end
  end

  # GET /concepts/new
  # GET /concepts/new.json
  def new
    @chef = Chef.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @chef }
    end
  end

  # GET /concepts/1/edit
  def edit
    @chef = Chef.find(params[:id])
  end

  # POST /concepts
  # POST /concepts.json
  def create
    @chef = Chef.new(params[:chef])

    respond_to do |format|
      if @chef.save
        format.html { redirect_success("Chefe criada com sucesso!",:chefs, :index)}
        format.json { render json: @chef, status: :created, location: @chef }
      else
        format.html { render action: "new" }
        format.json { render json: @chef.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /concepts/1
  # PUT /concepts/1.json
  def update
    @chef = Chef.find(params[:id])

    respond_to do |format|
      if @chef.update_attributes(params[:chef])
        format.html { redirect_success("Chefe alterada com sucesso!",:chefs, :index)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @chef.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /concepts/1
  # DELETE /concepts/1.json
  def destroy
    @chef = Chef.find(params[:id])
    @chef.destroy

    respond_to do |format|
      format.html { redirect_success("Chefe removida com sucesso!", :chefs, :index)}
      format.json { head :no_content }
    end
  end
end
