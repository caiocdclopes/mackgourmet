class IngredientsController < ApplicationController
  # GET /recipes
  # GET /recipes.json
  def index
    @ingredients = []
    if params[:recipe]
      @recipe = Recipe.find(params[:recipe])
      @ingredients = @recipe.ingredients 
    else
      @ingredients = Ingredient.all
    end
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
    @ingredient = Ingredient.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ingredient }
    end
  end

  # GET /recipes/new
  # GET /recipes/new.json
  def new
    @ingredient = Ingredient.new
    if params[:recipe]
      @recipe = Recipe.find(params[:recipe])
    else
      render_404
    end
  end

  # GET /recipes/1/edit
  def edit
    @ingredient = Ingredient.find(params[:id])
    @recipe = @ingredient.recipe
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @ingredient = Ingredient.new(params[:ingredient])
    respond_to do |format|
      if @ingredient.save
        format.html { redirect_success_index_show("Ingrediente criada com sucesso!",:recipes, @ingredient.recipe.id)}
        format.json { render json: @ingredient, status: :created, location: @ingredient }
      else
        format.html { render action: "new" }
        format.json { render json: @ingredient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recipes/1
  # PUT /recipes/1.json
  def update
    @ingredient = Ingredient.find(params[:id])

    respond_to do |format|
      if @ingredient.update_attributes(params[:ingredient])
        format.html { redirect_success_index_show("Ingrediente alterada com sucesso!",:recipes, @ingredient.recipe.id)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ingredient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @ingredient = Ingredient.find(params[:id])
    @ingredient.destroy

    respond_to do |format|
        format.html { redirect_success_index_show("Ingrediente removidao com sucesso!",:recipes, @ingredient.recipe.id)}
      format.json { head :no_content }
    end
  end
end
