class RestaurantsController < ApplicationController

  # GET /restaurants
  # GET /restaurants.json
  def index
    respond_to do |format|
      format.html 
      format.json { render json: RestaurantsDatatable.new(view_context, current_user) }
    end    
  end

  # GET /places/1
  # GET /places/1.json
  def show
    @restaurant = Restaurant.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: { profissionais: @restaurant.especialidades } }
    end
  end

  # GET /places/new
  # GET /places/new.json
  def new
    @restaurant = Restaurant.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @restaurant }
    end
  end

  # GET /places/1/edit
  def edit
    @restaurant = Restaurant.find(params[:id])
  end

  # POST /restaurants
  # POST /restaurants.json
  def create

    @restaurant = Restaurant.new(params[:restaurant])
    
    respond_to do |format|
      if @restaurant.save
        format.html { redirect_success("REstaurante criada com sucesso!",:restaurants, :index)}
        format.json { render json: @restaurant, status: :created, location: @restaurant }
      else
        format.html { render action: "new" }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /places/1
  # PUT /places/1.json
  def update
    @restaurant = Restaurant.find(params[:id])
    respond_to do |format|
      if @restaurant.update_attributes(params[:restaurant])
        @restaurant.save
        format.html { redirect_success("REstaurante alterada com sucesso!",:restaurants, :index)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @restaurant = Restaurant.find(params[:id])
    @restaurant.destroy

    respond_to do |format|
        format.html { redirect_success("Empresa removida com sucesso!",:restaurants, :index)}
      format.json { head :no_content }
    end
  end
end
