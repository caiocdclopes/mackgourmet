class RecipePhotosController < ApplicationController
  # GET /recipePhotos
  # GET /recipePhotos.json
  def index
    @recipePhotos = []
    if params[:recipe]
      @recipe = Recipe.find(params[:recipe])
      @recipePhotos = @recipe.recipePhotos 
    else
      @recipePhotos = RecipePhoto.all
    end
  end

  # GET /recipePhotos/1
  # GET /recipePhotos/1.json
  def show
    @recipePhoto = RecipePhoto.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @recipePhoto }
    end
  end

  # GET /recipePhotos/new
  # GET /recipePhotos/new.json
  def new
    @recipePhoto = RecipePhoto.new
    if params[:recipe]
      @recipe = Recipe.find(params[:recipe])
    else
      render_404
    end
  end

  # GET /recipePhotos/1/edit
  def edit
    @recipePhoto = RecipePhoto.find(params[:id])
    @recipe = @recipePhoto.recipe
  end

  # POST /recipePhotos
  # POST /recipePhotos.json
  def create
    @recipePhoto = RecipePhoto.new(params[:recipe_photo])
    respond_to do |format|
      if @recipePhoto.save
        format.html { redirect_success_index_show("RecipePhoto criada com sucesso!",:recipePhotos, @recipePhoto.recipe.id)}
        format.json { render json: @recipePhoto, status: :created, location: @recipePhoto }
      else
        format.html { render action: "new" }
        format.json { render json: @recipePhoto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /recipePhotos/1
  # PUT /recipePhotos/1.json
  def update
    @recipePhoto = RecipePhoto.find(params[:id])

    respond_to do |format|
      if @recipePhoto.update_attributes(params[:recipe_photo])
        format.html { redirect_success_index_show("RecipePhoto alterado com sucesso!",:recipePhotos, @recipePhoto.recipe.id)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recipePhoto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipePhotos/1
  # DELETE /recipePhotos/1.json
  def destroy
    @recipePhoto = RecipePhoto.find(params[:id])
    @recipePhoto.destroy

    respond_to do |format|
      format.html { redirect_success_index_show("RecipePhoto removido com sucesso!",:recipePhotos, @recipePhoto.recipe.id)}
      format.json { head :no_content }
    end
  end
end
