class LoginController <  ActionController::Base

	def login
		u = User.where(:email => params[:email]).first
 		if !u.nil? && u.valid_password?(params[:password])
			sign_in u, :bypass => true
			u.save(validate: false)
			render :json =>  {:status => u.id, :type => u._type}.to_json
		else
			render :json =>  {:status => "error"}.to_json
		end
	end
	
	def recovery
      user = User.where(email: params[:email])
      if user.count > 0
        user = user.first
        o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
        string = (0...8).map { o[rand(o.length)] }.join
        user.password = string
        user.password_confirmation = string
        MackGourmetmailer.recovery(user.email,string).deliver
        user.save
        render :json => {status: "Sua senha acabou se der enviada para seu e-mail."}
      else
        render :json => {status: "email não encontrado"}
    end
  end

  def newAccount
    u = User.where(:email => params[:email]).first
    if !u.nil?
      if u.valid_password?(params[:password]) ||  params[:facebook] != "empty"
        sign_in u, :bypass => true
        u.save(validate: false)
        render :json =>  {:status => u.id, :type => u._type}.to_json
      else
        render :json =>  {:status => "error"}.to_json
      end
    else
      user = User.new
      if params[:facebook] != "empty"
        o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
        string = (0...8).map { o[rand(o.length)] }.join
        user.password = params[:facebook]
        user.password_confirmation = params[:facebook]
        user.facebook = params[:facebook]
      else
        user.password = params[:password]
        user.password_confirmation = params[:password]
      end
      user.name = params[:name]
      user.email = params[:email]
      user.save(validate: false)
      sign_in user, :bypass => true
      render :json =>  {:status => user.id}.to_json
    end
  end



end