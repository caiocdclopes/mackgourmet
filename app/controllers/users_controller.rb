class UsersController < ApplicationController
  # GET /concepts
  # GET /concepts.json
  def index
    @users = User.where(:_type => "User")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @concepts }
    end
  end

  def edit 
    @user= current_user
  end

  # GET /concepts/1
  # GET /concepts/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def update
    @usuario = User.find(current_user.id)
    
    if current_user.isSuperAdmin? 
      p = params[:super_admin]
    elsif current_user.isStudent?
      p = params[:student]
    else
      p = params[:user]
    end

    if p[:password].nil?
      if @usuario.update_without_password(p)
        @usuario = User.find(current_user.id)
        redirect_success_home("Perfil com sucesso")
      else
       redirect_error("Por favor, preencha os dados obrigatórios corretamente.", :usuarios, :edit)
      end
    elsif p[:password] == p[:password_confirmation] && p[:password].length >= 8
        if @usuario.update_with_password(p)
          @usuario = User.find(current_user.id)
          sign_in @usuario, :bypass => true
          redirect_to("/alterar_senha", :flash => { :notice_success => "Senha alterada com sucesso." })   
        else
          redirect_to("/alterar_senha", :flash => { :notice_error => "Insira a senha atual corretamente." })   
        end
    elsif  p[:password] != p[:password_confirmation]
      redirect_to("/alterar_senha", :flash => { :notice_error => "A senhas não coincidem" })   
    else
      redirect_to("/alterar_senha", :flash => { :notice_error => "A nova senha deve ter 8 ou mais caracteres." }) 
    end
  end


  # DELETE /concepts/1
  # DELETE /concepts/1.json
  def destroy
    @concept = User.find(params[:id])
    @concept.destroy

    respond_to do |format|
        format.html { redirect_success("Usuário removido com sucesso!",:users, :index)}
      format.json { head :no_content }
    end
  end
end
