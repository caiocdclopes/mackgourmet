class RecipesController < ApplicationController

  # GET /recipes
  # GET /recipes.json
  def index
    respond_to do |format|
      format.html 
      format.json { render json: RecipesDatatable.new(view_context, current_user) }
    end    
  end

  # GET /recipe/1
  # GET /recipe/1.json
  def show
    @recipe = Recipe.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: { profissionais: @recipe } }
    end
  end

  # GET /places/new
  # GET /places/new.json
  def new
    @recipe = Recipe.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @recipe }
    end
  end

  # GET /places/1/edit
  def edit
    @recipe = Recipe.find(params[:id])
  end

  # POST /recipes
  # POST /recipes.json
  def create
    
    @recipe = Recipe.new(params[:recipe])
    @recipe.user = current_user
    respond_to do |format|
      if @recipe.save
        format.html { redirect_success("Receita criada com sucesso!",:recipes, :index)}
        format.json { render json: @recipe, status: :created, location: @recipe }
      else
        format.html { render action: "new" }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /places/1
  # PUT /places/1.json
  def update
    @recipe = Recipe.find(params[:id])
    respond_to do |format|
      if @recipe.update_attributes(params[:recipe])
        @recipe.save
        format.html { redirect_success("Loja alterada com sucesso!",:recipes, :index)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy

    @recipe = Recipe.find(params[:id])
    @recipe.destroy

    respond_to do |format|
        format.html { redirect_success("Empresa removida com sucesso!",:recipes, :index)}
      format.json { head :no_content }
    end
  end
end
