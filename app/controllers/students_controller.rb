class StudentsController < ApplicationController
  # GET /students
  # GET /students.json
  def index
    @students = Student.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @students }
    end
  end

  # GET /students/1
  # GET /students/1.json
  def show
    @student = Student.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @student }
    end
  end

  # GET /student/new
  # GET /student/new.json
  def new
    @student = Student.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @student }
    end
  end

  # GET /student/1/edit
  def edit
    @student = Student.find(params[:id])
  end

  # POST /student
  # POST /student.json
  def create
    @student = Student.new(params[:student])

    @recipe = Recipe.new(params[:recipe])
    @recipe.user = current_user

    respond_to do |format|
      if @student.save
        format.html { redirect_success("Estudante criado com sucesso!", :students, :index)}
        format.json { render json: @student, status: :created, location: @student }
      else
        format.html { render action: "new" }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /student/1
  # PUT /student/1.json
  def update
    @student = Student.find(params[:id])

    respond_to do |format|
      if @student.update_attributes(params[:student])
        format.html { redirect_success("Estudante alterado com sucesso!", :students, :index)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student/1
  # DELETE /student/1.json
  def destroy
    @student = Student.find(params[:id])
    @student.destroy

    respond_to do |format|
      format.html { redirect_success("Estudante removido com sucesso!", :students, :index)}
      format.json { head :no_content }
    end
  end
end
