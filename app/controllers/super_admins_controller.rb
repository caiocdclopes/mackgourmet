class SuperAdminsController < ApplicationController
  # GET /super_admins
  # GET /super_admins.json
  def index
    @super_admins = SuperAdmin.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @super_admins }
    end
  end

  # GET /super_admins/1
  # GET /super_admins/1.json
  def show
    @super_admin = SuperAdmin.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @super_admin }
    end
  end

  # GET /super_admins/new
  # GET /super_admins/new.json
  def new
    @super_admin = SuperAdmin.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @super_admin }
    end
  end

  # GET /super_admins/1/edit
  def edit
    @super_admin = SuperAdmin.find(params[:id])
  end

  # POST /super_admins
  # POST /super_admins.json
  def create
    @super_admin = SuperAdmin.new(params[:super_admin])
    # @super_admin.password = "12345678"
    # @super_admin.password_confirmation = "12345678"

    respond_to do |format|
      if @super_admin.save
        format.html { redirect_success("Admin criado com sucesso!",:super_admins, :index)}
        format.json { render json: @super_admin, status: :created, location: @super_admin }
      else
        format.html { render action: "new" }
        format.json { render json: @super_admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /super_admins/1
  # PUT /super_admins/1.json
  def update
    @super_admin = SuperAdmin.find(params[:id])

    respond_to do |format|
      if @super_admin.update_attributes(params[:super_admin])
        format.html { redirect_success("Admin alterado com sucesso!",:super_admins, :index)}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @super_admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /super_admins/1
  # DELETE /super_admins/1.json
  def destroy
    @super_admin = SuperAdmin.find(params[:id])
    @super_admin.destroy

    respond_to do |format|
      format.html { redirect_success("Admin removido com sucesso!",:super_admins, :index)}
      format.json { head :no_content }
    end
  end
end
