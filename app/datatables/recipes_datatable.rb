class RecipesDatatable
  delegate :params, :h, :link_to, to: :@view

    def initialize(view, user)
    @view = view
    @current_user = user
    if @current_user.isStudent?
      @source = Recipe.where(:user => @current_user.id)
    else 
      @source = Recipe.all
    end
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: extratos.count,
      iTotalDisplayRecords: extratos.count,
      aaData: data
    }
  end

private

  def data
    extratos.map do |extrato|
      [
        extrato.name,
        extrato.kind,
        extrato.concept.name,
        extrato.user.name,
        @view.layout_opts_stores(extrato,"recipes")
      ]
    end
  end

  def extratos
    @extratos ||= fetch_extratos
  end

  def fetch_extratos
    extratos = nil
    if params[:sSearch].present?
      @types_id = []
       extratos = @source.any_of({:name => /.*#{params[:sSearch]}.*/i},
                                 {:kind => /.*#{params[:sSearch]}.*/i},
                                 {:city => /.*#{params[:sSearch]}.*/i},
                                 {"concept.name" => /.*#{params[:sSearch]}.*/i},
                                 {"user.name" => /.*#{params[:sSearch]}.*/i},
                                 {:zip => /.*#{params[:sSearch]}.*/i}
                                 )       
       # extratos =  Extrato.all
    else
        extratos = @source.order_by("#{sort_column} #{sort_direction}")
    end
    extratos = extratos.paginate(:page => page, :limit => per_page)
    extratos
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 20
  end

  def sort_column
    columns = ["name","kind","concept.name", "user.name",  "id"]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == "desc" ? "desc" : "asc"
  end
end