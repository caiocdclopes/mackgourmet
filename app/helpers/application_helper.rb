﻿module ApplicationHelper
 
def br_states
  [
   ['São Paulo', 'SP'],
    ['Acre', 'AC'],
    ['Alagoas', 'AL'],
    ['Amapá', 'AP'],
    ['Amazonas', 'AM'],
    ['Bahia', 'BA'],
    ['Ceará', 'CE'],
    ['Distrito Federal', 'DF'],
    ['Espírito Santo', 'ES'],
    ['Goiás', 'GO'],
    ['Maranhão', 'MA'],
    ['Mato Grosso', 'MT'],
    ['Mato Grosso do Sul', 'MS'],
    ['Minas Gerais', 'MG'],
    ['Pará', 'PA'],
    ['Paraába', 'PB'],
    ['Paraná', 'PR'],
    ['Pernambuco', 'PE'],
    ['Piauí', 'PI'],
    ['Rio de Janeiro', 'RJ'],
    ['Rio Grande do Norte', 'RN'],
    ['Rio Grande do Sul', 'RS'],
    ['Rondônia', 'RO'],
    ['Roraima', 'RR'],
    ['Santa Catarina', 'SC'],
    ['Sergipe', 'SE'],
    ['Tocantins', 'TO']
  ]
  end
  
   def sexos
    [
      ['Masculino','Masculino'],
      ['Feminino','Feminino']
    ]
   end

  def br_bancos
    [
      ["Banco do Brasil S.A.", "001"],
      ["Banco Santander S.A.", "033"],
      ["Caixa Economica Federal", "104"],
      ["Banco Bradesco S.A.", "237"],
      ["Itau Unibanco S.A.", "341"],
      ["HSBC Bank Brasil S.A.", "399"],
      ["Banco Safra S.A.", "422"],
      ["Banco Citibank S.A.", "745"],
      ["Banco Cooperativo Sicredi S.A.", "748"],
      ["Banco Cooperativo do Brasil S.A.", "756"]
    ]
   end
  
  def transfers
    [
      ["Semanal", "weekly"],
      ["Mensal", "monthly"],
      ["Diário", "daily"]
    ]
   end

  def status
    [
      ["Ativo", "Ativo"],
      ["Cancelado", "Cancelado"],
      ["Bloqueado", "Bloqueado"]
    ]
   end

  
 def icons
    File.readlines('icons.txt')
  end

  def openwebicons
    File.readlines('openweb.txt')
  end

  def nav_active(options = {})
    cumbs = request.fullpath.to_s
    paths = cumbs.split("/")
    found = false
    paths.each do |path|
      if !options[:warning].blank? and options[:warning].to_s == path
        found = true
      end
    end
    if found
      return "active" 
    end
      return ""
  end

  def notice_helper
    notice = ""
    flash.each do |name, msg|       
      if name.to_s == "notice_success" and msg.length > 0
        notice += "<div class='alert alert-success alert-dismissable'>
             <i class='fa fa-check'></i>
             <b>Sucesso</b> 
             #{ msg }  
          </div> "
      elsif msg.length > 0
        notice += "<div class='alert alert-error alert-dismissable'>
             <i class='fa fa-close'></i>
             <b>Erro!</b> 
             #{ msg }  
          </div> "
        end
    end

     html = <<-HTML
              #{ notice }
               HTML
            html.html_safe
  end  


  def notice_helper_login
    notice = ""
    flash.each do |name, msg|
     puts name       
     puts msg 
     if  msg.length > 0 && msg == "Senha ou usuário inválido."    
        notice += " 
            <script>
            $( document ).ready(function() {
            alert('Senha ou usuário inválido.');
            });
            </script>
          "
      elsif msg.length > 0
        notice += " 
            <script>
            $( document ).ready(function() {
             alert('Em breve você vai receber um e-mail com as intruções.');
            });
            </script>
          "
        end
    end
     html = <<-HTML
              #{ notice }
               HTML
            html.html_safe
  end  


  def layout_opts (d,path)
    notice = '<div class="btn-group">
              <button type="button" class="btn btn-default">Opções</button>
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">'

       str = '"' + d.id + '"'
       tail = " </ul></div>"

       show =  "<li><a href='/#{path}/#{d.id}'><i class='fa fa-search'></i> Detalhes</a></li>"      
       edit =  "<li><a href='/#{path}/#{d.id}/edit'><i class='fa fa-pencil'></i> Editar</a></li>"      
       remove =  "<li><a href='/#{path}/#{d.id}' data-confirm='Deseja Remover o Item?'' data-method='delete' rel='nofollow'><i class='fa fa-trash-o'></i> Remover</a></li>"      
   

        html = <<-HTML
                #{ notice }
                #{ show }
                #{ edit }
                #{ remove }
                #{ tail }
                 HTML
        html.html_safe

    end

  def layout_opts_stores (d,path)
    notice = '<div class="btn-group">
              <button type="button" class="btn btn-default">Opções</button>
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
              </button>
              <ul class="dropdown-menu" role="menu">'

       str = '"' + d.id + '"'
       tail = " </ul></div>"


       hours =  "<li><a href='/ingredients?recipe=#{d.id}'><i class='fa fa-clock-o'></i> Ingredientes</a></li>"      
       combos =  "<li><a href='/recipe_methods?recipe=#{d.id}'><i class='fa fa-cutlery'></i> Métodos</a></li>"      
       recipePhotos = "<li><a href='/recipePhotos?recipe=#{d.id}'><i class='fa fa-picture-o'></i> Fotos</a></li>"      
       show =  "<li><a href='/#{path}/#{d.id}'><i class='fa fa-search'></i> Detalhes</a></li>"      
       edit =  "<li><a href='/#{path}/#{d.id}/edit'><i class='fa fa-pencil'></i> Editar</a></li>"      
       remove = "<li><a href='/#{path}/#{d.id}' data-confirm='Deseja Remover o Estabelecimento?'' data-method='delete' rel='nofollow'><i class='fa fa-trash-o'></i> Remover</a></li>"      
   

        html = <<-HTML
                #{ notice }
                #{ show }
                #{ hours }
                #{ combos }
                #{ recipePhotos }
                #{ edit }
                #{ remove }
                #{ tail }
                 HTML
        html.html_safe

    end


end
