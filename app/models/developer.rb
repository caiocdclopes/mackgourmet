class Developer
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Paperclip

  field :name, :type => String, :default => ""
  field :email,              :type => String, :default => ""
  field :description, type: String
  field :phone, type: String

  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'users',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

  validates_length_of :description, :maximum => 600
  validates_presence_of :email, :message => "digite um e-mail"
  validates_uniqueness_of :email, :case_sensitive => true, :message => "E-mail ja cadastrado"
  validates_format_of :email, :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i , :message => "e-mail invalido"

end
