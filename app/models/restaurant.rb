class Restaurant
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps
  include Geocoder::Model::Mongoid
  
  field :name, type: String
  field :email, type: String, :default => ""
  field :phone, type: String, :default => ""
  field :instagram_url, type: String, :default => ""
  field :description, type: String
  
  field :street, type: String
  field :neighborhood, type: String
  field :number, type: String
  field :zip, type: String, :default => ""
  field :city, type: String
  field :state, type: String
  field :icon, type: String
  field :complement, type: String
 
  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'mackgourmet',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')
   
  field :coordinates, :type => Array
  field :priceAvarage, :type => Integer

  belongs_to :concept
  has_one :chef

  validates_length_of :description, :maximum => 300
  validates_presence_of :name, :message => "Digite um nome para o restaurante."
  validates_presence_of :description, :message => "Digite uma descrição para o restaurante."
  validates_presence_of :concept, :message => "Escolha um conceito para o Restaurante."

  def self.mapRestaurants (array)
    array.map { |u| {
     :id => u.id,
     :name => u.name,
     :concept => {name: u.concept.name, id: u.concept.id},
     :coordinates => u.coordinates,
     :picture => u.picture.url,
     :neighborhood => u.neighborhood,
     :priceAvarage => u.priceAvarage,
     :address => [u.street, u.city, u.state, u.zip].compact.join(', ')
     }}
  end

  def latitude
    coordinates[1] # or coordinates[1] if you use the array
  end

  def longitude
    coordinates[0] # or coordinates[0] if you use the array
  end

end
