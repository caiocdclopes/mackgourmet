class Student < User

  belongs_to :concept
  validates_presence_of :concept, :message => "Escolha um conceito para o estudante."

end
