class Chef
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps
  include Geocoder::Model::Mongoid

  field :name, type: String
  field :about, type: String
 
  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'mackgourmet',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')
   
  has_many :recipes
  belongs_to :concept 
  belongs_to :restaurant

  validates_presence_of :concept, :message => "Você deve escolher um conceito para o Chefe."

end
