class RecipePhoto
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps

  field :name, type: String

  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'mackgourmet',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

  belongs_to :recipe

  def self.mapRecipePhotos (array)
    array.map { |u| {:id => u.id,
     :name => u.name,
     :subtitle => u.subtitle,
     :picture => u.picture.url
     }}
  end

end
