class Ingredient 
  include Mongoid::Document
  include Mongoid::Timestamps
  include Geocoder::Model::Mongoid

  field :enabled, type: Boolean, default: true  
  field :name, type: String
  field :amount, type: Float
  field :amount_unit, type: String
  field :to, type: String
  
  belongs_to :recipe
 
  def self.mapIngredients (array)
    array.map { |u| {
     :id => u.id,
     :name =>u.name,
     :amount =>u.name,
     :amount_unit =>u.amount_unit,
     }}
  end

end

