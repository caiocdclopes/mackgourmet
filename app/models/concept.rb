class Concept 
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps
  include Geocoder::Model::Mongoid

  field :name, type: String
  field :description, type: String
 
  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'mackgourmet',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')
  
  has_many :students
  has_many :recipes
  has_many :chefs
  has_many :restaurants

  validates_length_of :description, :maximum => 600
 
  def self.mapConcepts (array)
    array.map { |c| {
     :id => c.id,
     :name => c.name,
     :description => c.description,
     :picture => c.picture.url
     }}
  end
  
  def self.mapConcept(u)
     {
     :id => u.id,
     :name => u.name,
     :description => u.description,
     :picture => u.picture.url
     }
  end
  
end

