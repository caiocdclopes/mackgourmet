class RecipeMethod
  include Mongoid::Document
  include Mongoid::Timestamps

  field :step, type: String
  field :description, type: String

  belongs_to :recipe

  def self.mapRecipeMethods (array)
    array.map { |u| {:id => u.id,
     :step => u.step,
     :description => u.description
     }}
  end

end
