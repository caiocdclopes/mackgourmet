class Recipe 
  include Mongoid::Document
  include Mongoid::Paperclip
  include Mongoid::Timestamps
  
  field :name, type: String
  field :description, type: String
  field :kind, type: String

  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'mackgourmet',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

  belongs_to :concept
  belongs_to :user
  has_many :ingredients
  has_many :recipePhotos
  has_many :recipeMethods

  validates_presence_of :name, :message => "Digite um nome."
  validates_presence_of :description, :message => "Digite uma descrição para sua receita."

  def self.mapRecipes (array, current_user)
    array.map { |u| {
     :id => u.id,
     :name => u.name,
     :concept => {name: u.concept.name, id: u.concept.id},
     :picture => u.picture.url,
     :description => u.description,
     :address => [u.street, u.city, u.state, u.zip].compact.join(', '),
     :is_favorite => current_user.favorites.where(:recipe_id => u.id).count > 0
     }}
  end

  def self.mapRecipesMap (array)
    array.map { |u| {
     :id => u.id,
     :name => u.name,
     :current_recipe => getIngredient(u),
     :address => u.neighborhood,
     }}
  end

  def self.findRecipe (array, current_user)
    array.map { |u| {
     :id => u.id,
     :name => u.name,
     :concept => {name: u.concept.name, id: u.concept.id},
     :current_recipe => getIngredient(u),
     :picture => u.picture.url,
     :email => u.email,
     :zip => u.zip,
     :recipePhotos => RecipePhoto.mapRecipePhotos(u.recipePhotos),
     :description => u.description,
     :open_hours => Hour.mapHours(u.hours),
     :phone => u.phone,
     :has_rating => current_user.ratings.where(:recipe_id => u.id).count > 0,
     :url => u.url,
     :address => [u.street, u.city, u.state, u.zip].compact.join(', '),
     :is_favorite => current_user.favorites.where(:recipe_id => u.id).count > 0
     }}
  end

end