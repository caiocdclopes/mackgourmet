class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  include Mongoid::Paperclip

  devise :database_authenticatable,:registerable,
         :recoverable, :rememberable, :trackable, :validatable
  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  field :name, :type => String, :default => ""
  field :username, :type => String
  field :facebook, :type => String

  # validate :check_avatar
  validates_presence_of :name, :message => "digite um nome"
  validates_presence_of :email, :message => "digite um e-mail"
  validates_confirmation_of :password, :message => "As senhas não coincidem"
  validates_uniqueness_of :email, :case_sensitive => true, :message => "E-mail ja cadastrado"
  validates_format_of :email, :with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i , :message => "e-mail invalido"

  has_mongoid_attached_file :picture,
    :storage        => :s3,
    :bucket_name    => 'mackgourmet',
    :bucket         => 'users',
    :path           => ':attachment/:id/:style.:extension',
    :s3_credentials => File.join(Rails.root, 'config', 's3.yml')

   #relationships
   has_many :recipes

  def isAdmin
    self.class == Admin
  end

  def isSuperAdmin?
    self.class == SuperAdmin
  end

  def isUser?
    self.class == User
  end

  def isStudent?
    self.class == Student
  end

  def self.mapUser (u) { 
    :id => u.id,
    :name => u.name,
    :email => u.email,
    :picture => (u.facebook.nil? || u.changedPhoto) ?  u.picture.url(:original) : "http://graph.facebook.com/#{u.facebook}/picture?type=large",
    :type => u._type,
    :created_at => u.created_at.nil? ? "" : u.created_at
  }
  end

  def self.mapUser2 (u)
    { :email => u.email, :id => u.id,  :picture => (u.facebook.nil? || u.changedPhoto) ?  u.picture.url(:original) : "http://graph.facebook.com/#{u.facebook}/picture?type=large" , :name => u.name }
  end

end
