MackGourmetServer::Application.routes.draw do

  resources :restaurants
  resources :chefs
  resources :recipe_methods
  resources :recipes
  resources :ingredients
  resources :recipePhotos
  resources :recipe_photos
  resources :super_admins
  resources :students
  resources :developers

  apipie

  resources :concepts
  resources :admins
  devise_for :users
  resources :users

  root :to => "home#index"

  get "alterar_senha" => "users#edit"
  get "meu_perfil" => "users#edit"
  
  namespace :webservices do
    # Login
    post "login/signin"
    post "login/signinFacebook"
    post "login/signup"
    post "login/forgotPass"
    # Account
    post "account/updatePassword"
    get "account/getAbout"
    get "account/getNotifications"
    delete "account/deleteNotification"
    post "account/updatePhoto"
    put "account/updateAbout"
    # Concepts
    get "concepts/getConcepts"
    
    # get "places/getConcepts"
    # post "places/createFavorite"
    # delete "places/deleteFavorite"
    # get "places/getFavorites"
    # get "places/getClosestRecipes"
    # get "places/getRecipesByConcept"
    # get "places/findRecipe"
    # get "places/getIngredients"
    # get "places/getPayments"
    # get "places/getRatings"
    # post "places/createRating"
    # delete "places/deleteRating"
    # put "places/updateRating"
    # get "places/getRecipesMap"
 
    # get "orders/getChart"
    # get "orders/getMonths"
    # get "orders/getOrders"
    # get "orders/getMackGourmets"
    # get "orders/getChartConcepts"
    # get 'orders/getPendingMackGourmets'
    # get 'orders/getMackGourmetMonitor'

  end

  match ':controller(/:action(/:id))(.:format)'

end
