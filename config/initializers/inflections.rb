# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format
# (all these examples are active by default):
 ActiveSupport::Inflector.inflections do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
   inflect.uncountable %w( fish gestor admin_empresa adminEmpresa  coordenadorEstagio coordenador_estagio )
   inflect.irregular 'distribuidor', 'distribuidores'
   inflect.irregular 'evento', 'eventos'
   inflect.irregular 'funcionario', 'funcionarios'
   inflect.irregular 'categoria', 'categorias'
   inflect.irregular 'subcategoria', 'subcategorias'
   inflect.irregular 'monitorar', 'monitores'
   inflect.irregular 'profissional', 'profissionais'
   inflect.irregular 'cliente', 'clientes'
   inflect.irregular 'usuario', 'usuarios'
   inflect.irregular 'paciente', 'pacientes'
   inflect.irregular 'endereco', 'enderecos'
   inflect.irregular 'pedido', 'pedidos'
   inflect.irregular 'atendimento', 'atendimentos'
   inflect.irregular 'avaliacao', 'avaliacoes'
   inflect.irregular 'especialidade', 'especialidades'
   inflect.irregular 'pergunta', 'perguntas'
   inflect.irregular 'area', 'areas'
   inflect.irregular 'descricao', 'descricoes'
   inflect.irregular 'agenda', 'agendas'
   inflect.irregular 'operadora', 'operadoras'
   inflect.irregular 'admin', 'admins'
   inflect.irregular 'destaque', 'destaques'
   inflect.irregular 'loja', 'lojas'
   inflect.irregular 'ura', 'ura'
   inflect.irregular 'atendente', 'atendentes'
   inflect.irregular 'MackGourmet', 'MackGourmets'
   inflect.irregular 'item', 'itens'
 end
#
# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections do |inflect|
#   inflect.acronym 'RESTful'
# end
