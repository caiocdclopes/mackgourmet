# Be sure to restart your server when you modify this file.

MackGourmetServer::Application.config.session_store :cookie_store, key: '_rails3-mongoid-devise_session'
MackGourmetServer::Application.config.session_store :mongoid_store

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to recipe highly confidential information
# (create the session table with "rails generate session_migration")
# MackGourmetServer::Application.config.session_store :active_record_store
